// Отримати перше число від користувача
const userInput1 = prompt("Введіть перше число:");

// Отримати друге число від користувача
const userInput2 = prompt("Введіть друге число:");

// Отримати математичну операцію від користувача
const operation = prompt("Введіть математичну операцію (+, -, *, /):");

// Перетворити введені рядки на числа
const number1 = parseFloat(userInput1);
const number2 = parseFloat(userInput2);

// Перевірити, чи введені значення є числами
if (isNaN(number1) || isNaN(number2)) {
  console.log("Введено некоректні числа");
} else {
  // Функція для виконання математичних операцій
  function performOperation(a, b, op) {
    switch (op) {
      case "+":
        return a + b;
      case "-":
        return a - b;
      case "*":
        return a * b;
      case "/":
        return a / b;
      default:
        return NaN;
    }
  }

  // Виклик функції та виведення результату в консоль
  const result = performOperation(number1, number2, operation);
  if (isNaN(result)) {
    console.log("Введено некоректну операцію");
  } else {
    console.log("Результат:", result);
  }
}
